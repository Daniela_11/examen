from PyQt5 import QtWidgets, uic
from bien import VentanaEntrar
from ingreso_mal import VentanaError

class VentanaLogin(QtWidgets.QWidget):
    def __init__(self):
        super(VentanaLogin, self).__init__()
        uic.loadUi("registro.ui", self)
        self.enviar.clicked.connect(self.gui_login)
def gui_login(self):
        nombre = self.usuario.text()
        password = self.contra.text()
        if len(nombre) == 0 or len(password) == 0:
            self.mensaje_error.setText("Ingrese todos los datos")
        elif nombre == "dani" and password == "1234":
            self.gui_entrar()
        else:
            self.gui_error()

def gui_entrar(self):
        self.hide()
        VentanaEntrar.show()

def gui_error(self):
     self.hide()
     VentanaError.show()
