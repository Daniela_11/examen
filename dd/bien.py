from PyQt5 import QtWidgets, uic
from registro import VentanaLogin

class VentanaEntrar(QtWidgets.QWidget):
    def __init__(self):
        super(VentanaEntrar, self).__init__()
        uic.loadUi("ingreso_bien.ui", self)
        self.regresar1.clicked.connect(self.regresar_entrar)

    def regresar_entrar(self):
        self.hide()
        ventana_login = VentanaLogin()
        ventana_login.show()
