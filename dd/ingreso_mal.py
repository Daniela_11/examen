from PyQt5 import QtWidgets, uic
from registro import VentanaLogin

class VentanaError(QtWidgets.QWidget):
    def __init__(self):
        super(VentanaError, self).__init__()
        uic.loadUi("ingreso_mal.ui", self)
        self.regresar2.clicked.connect(self.regresar_error)

    
    def regresar_error(self):
        self.hide()
        ventana_login = VentanaLogin()
        ventana_login.show()