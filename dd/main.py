import sys
from PyQt5 import QtWidgets
from registro import VentanaLogin
from bien import VentanaEntrar
from ingreso_mal import VentanaError

# Iniciar app
app = QtWidgets.QApplication([])

# Crear instancias de ventanas
ventana_login = VentanaLogin()
ventana_entrar = VentanaEntrar()  # Cambiado el nombre del import
ventana_error = VentanaError()

# Ejecutable
ventana_login.show()
sys.exit(app.exec_())
