from cuenta import Cuenta
from propietario import Propietario

class CuentaCorriente(Cuenta):
    def __init__(self, id: int, n_cuenta: str, info_propietario: Propietario, saldo_cuenta: float, n_cheques:int) -> None:
        super().__init__(id, n_cuenta, info_propietario, saldo_cuenta)
        self.__n_cheques = n_cheques

    def emitir_cheque(self, monto: float) -> None:
        if monto <= self._saldo_cuenta :
            self.__n_cheques+=1
            self._saldo_cuenta = self._saldo_cuenta - monto
        else:
            print("el valor del cheque supera el monto de la cuenta")

    def get_n_cheques(self):
        return self.__n_cheques
    