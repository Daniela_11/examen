from cuenta import Cuenta
from propietario import Propietario

class CuentaAhorros(Cuenta):
    def __init__(self, id: int, n_cuenta: str, info_propietario: Propietario, saldo_cuenta: float, limite_transaccion:float) -> None:
        super().__init__(id, n_cuenta, info_propietario, saldo_cuenta)
        self.__limite_transaccion = limite_transaccion
       
    def validar_transaccion(self, monto):
        if monto <= self.__limite_transaccion:
            super().retiro(monto)
        else:
            print("La transacción excede el límite diario permitido")

        
    def get_limite_transaccion(self):
        return self.__limite_transaccion

    def set_limite_transaccion(self, limite_transaccion):
       self.__limite_transaccion = limite_transaccion
       
    