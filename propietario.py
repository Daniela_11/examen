from datetime import date

class Propietario :
    def __init__(self, id: int, nombre: str, apellido:str, n_documento:str, fecha_nacimiento:date, direccion:str, email:str) -> None:
        self.__id = id
        self.__nombre = nombre
        self.__apellido = apellido
        self.__n_documento = n_documento
        self.__fecha_nacimiento = fecha_nacimiento
        self.__direccion = direccion
        self.__email = email

    def __str__(self) -> str:
        return (f"Id: {self.__id}\n"
                f"Nombre: {self.__nombre}\n"
                f"Apellido: {self.__apellido}")   

    def get_id(self):
        return self.__id
    
    def set_id(self, id ):
        self.__id = id

    def get_nombre(self):
        return self.__nombre
    
    def set_nombre(self, nombre):
        self.__nombre = nombre

    def get_apellido(self):
        return self.__apellido
    
    def set_apellido(self, apellido ):
        self.__apellido = apellido

    def get_n_documento(self):
        return self.__n_documento
    
    def set_n_documento(self, n_documento):
        self.__n_documento = n_documento

    def get_fecha_nacimiento(self):
        return self.__fecha_nacimiento
    
    def set_fecha_nacimiento(self, fecha_nacimiento ):
        self.__fecha_nacimiento = fecha_nacimiento

    def get_direccion(self):
        return self.__direccion
    
    def set_direccion(self, direccion):
        self.__direccion = direccion

    def get_email(self):
        return self.__email
    
    def set_email(self, email ):
        self.__email = email


