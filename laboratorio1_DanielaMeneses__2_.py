print("-----Menú de opciones-----","\n","1.Suma","\n","2.Resta","\n","3.Multiplicación","\n","4.División","\n", "5.Potencia","\n","6.Raíz","\n","7.Salir")

opcion = float(input("Elija la operación que desea realizar: "))


match opcion:
    case 1:
        print("Usted ha seleccionado suma")
        a = float(input("ingrese el primer número que desea sumar: "))
        b = float(input("ingrese el segundo número que desea sumar: "))
        print("La suma de los números es: ", a+b)
    case 2:
        print("Usted ha seleccionado resta")
        c = float(input("ingrese el minuendo: "))
        d = float(input("ingrese el sustraendo: "))
        print("La resta de los números es: ", c-d)
    case 3:
        print("Usted ha seleccionado multiplicación")
        e = float(input("ingrese el multiplicando: "))
        f = float(input("ingrese el multiplicador: "))
        print("La multiplicación de los números es: ", e*f)
    case 4:
        print("Usted ha seleccionado división")
        g = float(input("ingrese el dividendo: "))
        h = float(input("ingrese el divisor: "))
        print("La división de los números es: ", g/h)
    case 5:
        print("Usted ha seleccionado potencia")
        i = float(input("ingrese la base: "))
        j = float(input("ingrese el exponente: "))
        print("El resultado es: ", i**j)
    case 6:
        print("Usted ha seleccionado raiz")
        k = float(input("ingrese el radicando: "))
        m = float(input("ingrese el índice: "))
        print("El resultado es: ", k**(1/m))
    case 7:
        print("Usted esta saliendo del menú")
        
    case _:
        print("Esta opción no existe")


