from datetime import date
from propietario import Propietario
from cuenta_ahorros import CuentaAhorros
from cuenta_corriente import CuentaCorriente

# Crear un propietario
juan_perez = Propietario(id=1, nombre="Juan", apellido="Perez", n_documento="123456789", fecha_nacimiento= date(2000,1,1), direccion="Calle 123", email="juan@example.com")

# Crear una cuenta corriente
cuenta_corriente = CuentaCorriente(id=2, n_cuenta="987654321", info_propietario= juan_perez, saldo_cuenta= 3254,  n_cheques=3)

# crear una cuenta de ahorro

cuenta_ahorros = CuentaAhorros(id= 3, n_cuenta="33493492", info_propietario= juan_perez, saldo_cuenta= 2000,limite_transaccion= 5000)

print("----------propietario----------")
print(juan_perez)

print("----------saldo de las cuentas----------")
print("saldo cuenta ahorro:",cuenta_ahorros.get_saldo_cuenta())
print("saldo cuenta corriente:",cuenta_corriente.get_saldo_cuenta(),"\n")

print("----------Deposito----------")
cuenta_corriente.deposito(232)
print("Saldo nuevo cuenta corriente:",cuenta_corriente.get_saldo_cuenta(),"\n")

print("----------Retiro----------")
cuenta_ahorros.retiro(698)
print("Saldo nuevo cuenta ahorros:",cuenta_ahorros.get_saldo_cuenta(),"\n")

print("----------restriccion cuenta de ahorros----------")
cuenta_ahorros.validar_transaccion(455657)
print("Saldo cuenta ahorro:",cuenta_ahorros.get_saldo_cuenta(),"\n")

print("----------Comprobación cheques cuenta corriente----------")
print("numero de cheques emitidos: ",cuenta_corriente.get_n_cheques(),"\n")

cuenta_corriente.emitir_cheque(33)

print("Saldo cuenta corriente:",cuenta_corriente.get_saldo_cuenta(),"\n")
print("numero de cheques emitidos: ",cuenta_corriente.get_n_cheques())