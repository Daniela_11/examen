from propietario import Propietario

class Cuenta:
    def __init__(self, id: int, n_cuenta: str, info_propietario: Propietario, saldo_cuenta: float) -> None:
        self.__id = id
        self.__n_cuenta = n_cuenta
        self.__info_propietario :Propietario = info_propietario
        self._saldo_cuenta = saldo_cuenta

        if not isinstance(self.__info_propietario, Propietario):
            raise ValueError("Tienes que ingresar un propietario, no un str, \
                    no un int")
    
        if saldo_cuenta < 0:
            raise ValueError ("El saldo de la cuenta no puede ser negativo")
    
    def deposito (self,monto):
        self._saldo_cuenta = self._saldo_cuenta + monto
        return self._saldo_cuenta
    
    def retiro (self ,monto):
        if self._saldo_cuenta >= monto:
           self._saldo_cuenta = self._saldo_cuenta - monto
           return self._saldo_cuenta
        else:
            print("Saldo insuficiente para realizar el retiro.")
            
    def consulta_saldo (self):
        return self._saldo_cuenta

    def get_id(self):
        return self.__id

    def set_id(self, id):
        self.__id = id

    def get_n_cuenta(self):
        return self.__n_cuenta

    def set_n_cuenta(self, n_cuenta):
        self.__n_cuenta = n_cuenta

    def get_info_propietario(self):
        return self.__info_propietario   

    def set_info_propietario(self, info_propietario):
        self.__info_propietario = info_propietario

    def get_saldo_cuenta(self):
        return self._saldo_cuenta

    def set_saldo_cuenta(self, saldo_cuenta):
        self._saldo_cuenta = saldo_cuenta




