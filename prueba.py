import sys
from PyQt5 import QtWidgets, uic

# Iniciar app
app = QtWidgets.QApplication([])

# Cargar archivos .ui
login = uic.loadUi("registro.ui")
entrar = uic.loadUi("ingreso_bien.ui")
error = uic.loadUi("ingreso_mal.ui")

# Función para el login
def gui_login():
    nombre = login.usuario.text()
    password = login.contra.text()
    if len(nombre) == 0 or len(password) == 0:
        login.mensaje_error.setText("Ingrese todos los datos")
    elif nombre == "dani" and password == "1234":
        gui_entrar()
    else:
        gui_error()

def gui_entrar():
    login.hide()
    entrar.show()

def gui_error():
    login.hide()
    error.show()

def regresar_entrar():
    entrar.hide()
    login.show()
def regresar_error():
    error.hide()
    login.show()



# Botones
login.enviar.clicked.connect(gui_login)  # Conectar el botón a la función
entrar.regresar1.clicked.connect(regresar_entrar)
error.regresar2.clicked.connect(regresar_error)



# Ejecutable
login.show()
sys.exit(app.exec_())
